import { BMTCore, BMTContent } from './index.js'

export class BMTModule extends BMTCore{
	constructor(...args){
		super()
		this.params={}
		this.tabs=[]
		this.currentTab=[]
		this.simpleLayout={}
		this.functions=[]
		this.params.subscribes=[]
		this.parent=args[0]
	}
	getGenerator(){
		return this.parent
	}
	setTitle(...args){
		let params={}
		let titleParams={}
		if(!args||args.lenght===0){
			throw new Error('Es necesario agregar parametros')
		}
		this.params.titleOptions=function(routeParams, routeQuery){
			if(typeof args[0] === 'function'){
				titleParams = args[0](routeParams, routeQuery)
			}else{
				titleParams = args[0]
			}
			params.title=titleParams.title
			if(titleParams.template){
				// params.template=titleParams.template
				params=titleParams
			}else{
				if(!titleParams.title){
					throw new Error('Es necesario ingresar titulo')
				}
				params.template='simpleListTitle'
			}
			return params
		}
		return this
	}
	newContent(...args){
		let tab = new BMTContent(this.tabs.length,this)
		this.tabs[this.tabs.length-1].push(tab)
		return tab
	}
	newTab(...args){
		let params={}
		let content = []
		let tab = new BMTContent(this.tabs.length,this)
		content.push(tab)
		this.tabs.push(content)
		return tab
	}
}
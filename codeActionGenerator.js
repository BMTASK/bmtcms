import { BMTCore } from './index.js'
import { Meteor } from 'meteor/meteor'
import { actionGeneratorErrors, generalErrors } from './codeErrors.js'
import { downloadTableExcel } from 'meteor/bmt:exportcsv'
import { afImageFileHook } from './utils.js'
import __ from 'lodash'
import Papa from 'papaparse'

export class BMTAction extends BMTCore{
	constructor(...args){
		super()
		this.params={}
		if(!args||args.length===0){
			throw new Error('Es necesario ingresar identificador unico al contenido')
		}
		this.metaParams={}
		this.metaParams.modalId=args[0]
		this.parent=args[1]
		this.metaParams.collection=this.parent.params.collection||''
		this.agregarFunction=false
		this.metaParams.actionId='js-modal-'+this.parent.getModule().getGenerator().uniqueId+'-'+this.parent.params.tabId+'-'+this.metaParams.modalId
		this.metaParams.globalFuncRef=this.metaParams.actionId
		this.metaParams.funcCount=0
		this.metaParams.funcRef={}
		this.metaParams.isEdit=false
		this.params.modal={}
		this.params.contentOptions={}
		this.functionParams=false
        // this.params.selector=this.params.actionId
        // this.params.cancelarSelector=this.params.actionId+"-cancelar"
        // this.params.aceptarSelector=this.params.actionId+"-aceptar"
	}
	getParams(routeParams, routeQuery){
		let params={}
		if(this.functionParams){
			params=this.functionParams(routeParams, routeQuery)
			params=__.assignIn(this.params.modal,params.modal)
		}else{
			params=this.params.modal
		}
		return params
	}
	getEdit(){
		this.metaParams.isEdit=true
		return this
	}
	getUniqueFuncName(){
		let funcName = this.metaParams.globalFuncRef+'-unique'+this.metaParams.funcCount
		this.metaParams.funcCount++
		return funcName
	}
	setAutoFormHook(){
		if(Meteor.isClient){
			let params = {}
			params[this.metaParams.actionId]=afImageFileHook(this.metaParams.collection, this.metaParams.schema)
			this.AutoForm.hooks(params)
		}
	}
	agregar(...args){
		let params={}
		if(!args||args.length===0){
			throw new Error('Es necesario ingresar al menos un parametro a la accion de agregar')
		}
		if(args[1]){
			params=args[1]
		}
		if(!params.collection&&this.metaParams.collection===''){
			throw new Error('Es necesario ingresar la informacion de la coleccion')
		}
		if(params.collection){
			this.metaParams.collection=params.collection
		}
		this.params.modal={}
		this.metaParams.schema=params.schema||this.metaParams.collection.slice(0, -1)
		this.metaParams.collection=params.collection||this.metaParams.collection
		this.setAutoFormHook()
		this.params.modal={
			selector			: this.metaParams.actionId,
			columnType			: params.columnType || 'single',
			titulo				: params.titulo || '',
			textContent			: params.textContent || [],
			cancelarSelector	: this.metaParams.actionId+'-cancelar',
			aceptarSelector		: this.metaParams.actionId+'-aceptar',
			openOptions			: '',
			closeOptions		: '',
			autoForm			: {
				schema				: this.metaParams.schema,
				id					: params.id||this.metaParams.actionId,
				collection			: this.metaParams.collection,
				method				: params.method||'autoFormInsert',
				methodUpdate		: params.methodUpdate||'autoFormUpdate',
				omitFields			: '',
				omitFieldsUpdate	: [],
				reset				: !!params.reset,
				submitClass			: this.metaParams.actionId+'-submit',
				fields				: args[0],
			},
		}
		this.agregarFunction=true
		this.params.addBtn=(typeof params.addBtn==='undefined')?true:params.addBtn
		//si es tabla, poner el boton de agregar
		if(this.params.addBtn&&this.parent.params.contentType==='table'){
			const params={}
			params['click .'+this.metaParams.actionId+'-add']=this.metaParams.actionId
			this.parent.events(params)
			this.parent.params.contentOptions.tabularOptions.addBtn= {
				selector: this.metaParams.actionId+'-add',
				modal: this.metaParams.modalId
			}
		}
		return this
	}
	editar(...args){
		let params={}, customFunc
		if(!args||args.length===0){
			throw new Error('Es necesario ingresar al menos un parametro a la accion de editar')
		}
		if(!this.agregarFunction){
			//generar una funcion de agregar vacia
			this.agregar(...args)
			// throw new Error('No es posible utilizar "editar" sin antes usar "agregar"')
		}
		if(args[0] && args[0].lenght !==0){
			this.params.modal.autoForm.updateFields=args[0]
		}
		if(args[1]){
			params=args[1]
		}
		if(args[2]){
			customFunc=args[2]
		}
		this.metaParams.funcRef.edit=this.metaParams.globalFuncRef+'-edit'
		this.parent.params.contentOptions.customEvent=this.metaParams.funcRef.edit
		//agregar la funcion de editar cuando es tabla
		if(this.parent.params.contentType==='table'&&params.selector){
			this.parent.params.contentOptions.tabularOptions.events.push({
				selector	: params.selector,
				name		: this.metaParams.funcRef.edit,
			})
		}
		//console.log(this.parent.getModule())
		if(customFunc){
			this.parent.getModule().functions.push({
				name:this.metaParams.funcRef.edit,
				function:customFunc,
			})
		}else{
			let actionId = this.metaParams.actionId
			if(this.parent.params.contentType==='table'){
				this.parent.getModule().functions.push({
					name:this.metaParams.funcRef.edit,
					function:function(e, instance) {
						e.preventDefault()
						let modalId = '.'+actionId
						const modal = findOwnContext(document.querySelector(modalId)),
							context = findOwnContext(e.target)
						modal.doc(context.data.self)
						modal.editAutoForm(true)
						Meteor.defer(() => {
							modal.modalReady(true)
							$(modalId).openModal()
						})
					},
				})
			}else{
				this.parent.getModule().functions.push({
					name:this.metaParams.funcRef.edit,
					function:function(e, instance) {
						e.preventDefault()
						let modalId = '.'+actionId
						const modal = findOwnContext(document.querySelector(modalId)),
							context = findOwnContext(e.target)
						modal.doc(context.info())
						modal.editAutoForm(true)
						Meteor.defer(() => {
							modal.modalReady(true)
							$(modalId).openModal()
						})
					},
				})
			}
			
		}
		this.params.modal.editTitulo=params.titulo||''
		this.params.modal.autoForm.schemaUpdate=params.schema||this.params.modal.autoForm.schema
		this.params.modal.autoForm.methodUpdate=params.method||this.params.modal.autoForm.methodUpdate
		return this
	}
	modal(...args){
		let params={}
		if(!args||args.length===0){
			throw new Error('Es necesario ingresar al menos un parametro a la accion de agregar')
		}
		if(args[0]){
			params=args[0]
		}
		this.params.modal={}
		this.params.modal={
			selector			: this.metaParams.actionId,
			columnType			: params.columnType || 'single',
			titulo				: params.titulo || '',
			textContent			: params.textContent || [],
			cancelarSelector	: this.metaParams.actionId+'-cancelar',
			aceptarSelector		: this.metaParams.actionId+'-aceptar',
			openOptions			: '',
			closeOptions		: ''
		}
		if(params.autoForm){
			this.params.modal.autoForm=params.autoForm
		}
		return this
	}
	onConfirm(...args){
		let params={}
		if(!args||args.length===0){
			throw new Error('Es necesario ingresar al menos un parametro a la accion de agregar')
		}
		if(args[0]){
			params=args[0]
		}
		if(!this.params.modal){
			throw new Error('Es necesario primero ingresar un Modal o Accion')
		}
		this.parent.getModule().functions.push({
			name:this.metaParams.globalFuncRef+'-onConfirm',
			function:params,
		})
		// little hack para que funcione, revisar la integracion con autoform
		if(!(this.params.modal && this.params.modal.autoForm)){
			this.params.modal.aceptarEvent=this.metaParams.globalFuncRef+'-onConfirm'
		}
		let eventParam = {}
		eventParam['click .autoform-'+this.metaParams.actionId+'-aceptar']=params
		this.events(eventParam)

		return this
	}
	onCancel(...args){
		let params={}
		if(!args||args.length===0){
			throw new Error('Es necesario ingresar al menos un parametro a la accion de agregar')
		}
		params['click .'+this.metaParams.actionId+'-cancelar']=args[0]
		this.events(params)
		params={}
		params['click .autoform-'+this.metaParams.actionId+'-cancelar']=args[0]
		this.events(params)
		return this
	}
	/** ToDo */
	onOpen(...args){
		let params={}
		if(!args||args.length===0){
			throw new Error('Es necesario ingresar al menos un parametro a la accion de onOpen')
		}
		if(typeof args[0] !== 'function'){
			throw new Error('Es necesario ingresar una funcion como parametro')
		}
		this._onOpen=args[0]
		return this
	}
	_onOpen(modal, args){
		return modal
	}
	closeModal(){
		Meteor.defer(()=>{
			$('.'+this.metaParams.actionId).closeModal()
		})
	}
	openModal(...args){
		let params = {}
		if(args&&args.lenght>1){
			params = args[0]
		}
		let modalId = '.'+this.metaParams.actionId
		let modal = findOwnContext(document.querySelector(modalId))
		const modalOnOpen = this._onOpen(modal, args)
		if(modalOnOpen){
			modal=modalOnOpen
		}
		Meteor.defer(() => {
			modal.modalReady(true)
			$(modalId).openModal(params)
		})
	}
	tabularButton(...args){
		let params = {}
		if(!args||args.lenght===0){
			throw new Error(generalErrors.noParams)
		}
		params = args[0]
		if(this.parent.params.contentType&&this.parent.params.contentType==='table'){
			if(!this.parent.params.contentOptions.tabularOptions.moreBtns){
				this.parent.params.contentOptions.tabularOptions.moreBtns=[]
			}
			const buttonParams = {
				iconType        : true,
				icon            : params.icon||'add',
				target          : this.metaParams.actionId+'-tabularButton',
				klass           : '',
				function        : this.metaParams.globalFuncRef+'-tabularButton',
				// realFunction    : this.metaParams.globalFuncRef+'-tabularButton',
				tooltip:{
					contentText: params.tooltip || ''
				}
			}
			this.parent.params.contentOptions.tabularOptions.moreBtns.push(buttonParams)
			if(!!args[1] && typeof args[1] === 'function'){
				this.parent.getModule().functions.push({
					name:this.metaParams.globalFuncRef+'-tabularButton',
					function:args[1],
				})
			}else if(typeof args[1] === 'object' && args[1].constructor === BMTAction){
				this.parent.getModule().functions.push({
					name:this.metaParams.globalFuncRef+'-tabularButton',
					function:function(e, instance) {
						e.preventDefault()
						args[1].openModal()
					},
				})
			}
		}else{
			throw new Error('Solo se puede agregar botones a los contenidos tipo tabla')
		}
		return this
	}
	excelDownloadButton(...args){
		let funcMap = false
		let options = {}
		if(args[0] && typeof args[0] === 'function'){
			funcMap = args[0]
			if(args[1] && typeof args[1] === 'object'){
				options = {}
			}
		}else if(args[0] && typeof args[0] === 'object'){
			options = args[0]
			if(args[1] && typeof args[1] === 'function'){
				funcMap = args[1]
			}
		}
		if(this.parent.params.contentType&&this.parent.params.contentType==='table'){
			this.tabularButton({
				icon:'file_download',
				tooltip: options.tooltip || ''
			},function(e,instance){
				e.preventDefault()
				const tableID = $(e.target).parent().parent().parent().attr('id')
				downloadTableExcel(tableID, funcMap, options)
			})
		}else{
			throw new Error(actionGeneratorErrors.excelDownloadNoTable)
		}
		return this
	}
	/** ToDo */
	excelImportButton(...args){
		const self = this
		const params = {}
		if(!args||args.length===0){
			throw new Error('Es necesario ingresar al menos un parametro a la accion de excel import')
		}
		if(args.length>1){
			params.collection=args[0]
			params.importFunction=args[1]
		}else if(args.length<1&&typeof args[0]=== "Object"){
			params.collection=this.metaParams.collection
			params.importFunction=args[0]
		}else{
			throw new Error('parametros incorrectos a la accion de excel import')
		}
		// const importFunction = args[1]
		const importFunction = params.importFunction
		if(this.parent.params.contentType&&this.parent.params.contentType==='table'){
			this.setGlobalSchema('excelUpload')
			let agregarExcel = this.agregar(
				[
					'excel',
				],
				{
					titulo		: 'Subir archivo de Excel',
					schema		: 'excelUpload',
					reset       : false,
					addBtn		: false
				}
			).onConfirm(async (e,instance) => {
				const file = instance.find('input[type=file]')
				let context = {}
				if(args[2] && typeof args[2] === 'function'){
					context = await args[2](e, instance)
				}
				agregarExcel.closeModal()
				let row = ''
				let column = ''
				let realRow = {}
				realRow={}
				Papa.parse(file.files[0], {
					complete: function(results) {
						// console.log("Finished:", results.data);
						__.each(results.data, (dataRow, dataKey)=>{
							__.each(importFunction,(v,k)=>{
								// console.log(k, dataRow, dataKey)
								// console.log(importFunction[k])
								let realData = ""
								if(typeof importFunction[k]==="function"){
									realData = importFunction[k](dataRow, dataRow[k], context)
								}else{
									realData = dataRow
								}
								realRow[k]=realData
								//concatenar column
							})
						})
						
					}
				})
				//iterar por cada row
				// __.each(importFunction,(v,k)=>{
				// 	// console.log(k)
				// 	// console.log(importFunction[k])
				// 	let realData = ""
				// 	if(typeof importFunction[k]==="function"){
				// 		realData = importFunction[k](row,column)
				// 	}else{
				// 		realData = column
				// 	}
				// 	realRow[k]=realData
				// 	//concatenar column
				// })
				//insertar realRow a la base de datos
				return
			})
			this.tabularButton({
				icon:'file_upload'
			},function(e,instance){
				e.preventDefault()
				self.openModal()
			})
		}else{
			throw new Error(actionGeneratorErrors.excelDownloadNoTable)
		}
		return this
	}
	tabularSearch(...args){
		let params = {}
		if(!args||args.lenght===0){
			throw new Error('Es necesario agregar parametros')
		}
		params = args[1]
		if(!this.params.modal){
			this.params.modal={}
		}
		if(this.parent.params.contentType&&this.parent.params.contentType==='table'){
			this.parent.params.contentOptions.tabularOptions.filterBtn = {
				selector: this.metaParams.actionId+'-filter',
				modal: this.metaParams.modalId
			}
			this.params.modal={
				selector			: this.metaParams.actionId,
				columnType			: params.columnType || 'single',
				titulo				: params.titulo || 'Busqueda',
				cancelarSelector	: this.metaParams.actionId+'-cancelarBusqueda',
				aceptarSelector		: this.metaParams.actionId+'-aceptarBusqueda',
				openOptions			: '',
				closeOptions		: '',
				autoForm			: {
					schema				: (params.schema||this.metaParams.collection)+'Busqueda',
					id					: (params.schema||this.metaParams.collection)+'Busqueda',
					collection			: params.collection||this.metaParams.collection,
					omitFields			: '',
					reset				: false,
					tabular             : this.parent.params.contentOptions.id,
					search				: true,
					searchClass			: this.metaParams.actionId+'-search',
					fields				: args[0],
				},
			}
		}else{
			throw new Error('Solo se puede agregar busqueda a los contenidos tipo tabla')
		}
		return this
	}
	/** ToDo */
	tabularDateFilter(...args){
		if(!args||args.lenght===0){
			throw new Error(generalErrors.noParams)
		}
		if(this.parent.params.contentType&&this.parent.params.contentType==='table'){
			if(!this.parent.params.contentOptions.tabularOptions.searchDate){
				this.parent.params.contentOptions.tabularOptions.searchDate=args[0]
			}
		}else{
			throw new Error(actionGeneratorErrors.onlyTable)
		}
		return this
	}
	/** ToDo */
	tabularDropDownSelector(){

	}
	function(...args){
		let params = {}
		if(!args||args.lenght===0){
			throw new Error('Es necesario agregar parametros')
		}
		if(!this.params.modal){
			this.params.modal={}
		}
		if(!this.params.modal.functions||this.params.modal.functions.constructor!==Array){
			this.params.modal.functions=[]
		}
		let funcName = this.getUniqueFuncName()
		if(typeof args[0] === 'function'){
			this.params.modal.functions.push(funcName)
			this.parent.getModule().functions.push({
				name:funcName,
				function:args[0],
			})
		}
		return this
	}
	events(...args){
		let params = {}
		if(!args||args.lenght===0){
			throw new Error('Es necesario agregar parametros')
		}
		if(!this.params.modal){
			this.params.modal={}
		}
		if(!this.params.modal.events||this.params.modal.events.constructor!==Array){
			this.params.modal.events=[]
		}
		for(let arg in args){
			let event=args[arg]
			for(let func in event){
				let funcName = this.getUniqueFuncName()
				if(typeof event[func] === 'function'){
					this.params.modal.events.push({
						name		: Object.keys(event)[0],
						function	: funcName,
					})
					this.parent.getModule().functions.push({
						name: funcName,
						function: event[func]
					})
				}else if(typeof event[func] === 'string'){
					this.params.modal.events.push({
						name		: Object.keys(event)[0],
						modal		: event[func],
					})
				}else if(typeof event[func] === 'object' && event[func].constructor === BMTAction){
					this.params.modal.events.push({
						name		: Object.keys(event)[0],
						modal		: event[func].metaParams.actionId,
					})
				}
			}
		}
		//console.log(params.functions)
		return this
	}
}

import __ from 'lodash'
import { localSchema } from './utils.js'
import { codeGeneratorErrors } from './codeErrors.js'

var settings={}

export class BMTCore{
	constructor(){
		this.reInitSettings()
	}
	setSettings(paramSettings){
		__.assignIn(settings,paramSettings)
		this.reInitSettings()
	}
	reInitSettings(){
		__.assignIn(this, settings)
	}
	getSetting(setting){
		return settings[setting]
	}
	setGlobalSchema(key){
		if(this.Schemas){
			this.Schemas[key]=localSchema[key]
		}else{
			throw new Error(codeGeneratorErrors.noSchemasSettup)
		}
	}
}
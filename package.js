/* global Package, Npm */

Package.describe({
  name: 'bmt:bmtcms',
  summary: 'CMS BMT',
  version: '0.2.8',
  git: ''
});

Npm.depends({
  'lodash'    : '4.17.4',
  'uniqid'    : '5.0.3',
  'papaparse' : '4.6.0'
});

Package.onUse(function(api) {
  api.use([
    'check',
    'ecmascript',
    'underscore',
    'mongo',
    'blaze',
    'templating',
    'reactive-var',
    'tracker',
	'session',
	'bmt:exportcsv',
	'aldeed:autoform'
  ]);

  // api.add_files([
  //   'client/simpleList/simpleList.html',
  //   'client/simpleList/styles.html',
  // ], ['client']);

  api.mainModule('index.js', ['client','server']);

  api.export('BMT');
});


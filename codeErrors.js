export const actionGeneratorErrors={}
export const codeGeneratorErrors={}
export const generalErrors={}

// Action Generator Errors
actionGeneratorErrors.excelDownloadNoTable='Solo se puede agregar boton de excel a los contenidos tipo tabla'
actionGeneratorErrors.onlyTable='Solo se puede agregar la funcionalidad a una tabla'

// Core Code Generator Errors
codeGeneratorErrors.created='No se puede agregar el parametro despues de haber creado el modulo'
codeGeneratorErrors.noSchemasSettup='No se han configurado los schemas globales'

// General Errors
generalErrors.noParams='Es necesario agregar parametros'
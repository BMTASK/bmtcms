import { BMTCore, BMTAction } from './index.js'
import { generalErrors } from './codeErrors.js'
import { _ } from 'underscore'
import  __  from 'lodash'

export class BMTContent extends BMTCore{
	constructor(...args){
		super()
		this.params={}
		if(args.lenght<1){
			throw new Error('Es necesario ingresar identificador unico al contenido')
		}
		this.params.tabId='tab-table-'+args[0]
		this.params.selector='js-tab-'+this.params.tabId
		this.params.query={}
		this.params.serverQuery={}
		this.params.serverProjection={}
		this.params.projection={}
		this.params.modalOptions=[]
		this.params.subscribes=[]
		this.params.events=[]
		this.params.contentOptions={}
		this.params.functions=[]
		this.functionParams=[]
		this.actions=[]
		this.parent=args[1]
		this.metaParams={}
		this.metaParams.funcCount=0
	}
	getParams(routeParams, routeQuery){
		let params={}
		if(this.functionParams.length > 0){
			for(const functions of this.functionParams){
				let tempParams={}
				tempParams = functions(routeParams, routeQuery)
				params = __.assignIn(this.params, tempParams)
			}
		}else{
			params=this.params
		}
		return params
	}
	getModule(){
		return this.parent
	}
	newContent(){
		return this.parent.newContent()
	}
	cards(...args){
		let params={}
		if(!args||args.lenght===0){
			throw new Error('Es necesario agregar parametros')
		}
		let cardParams = args[0]
		if(!cardParams.collection){
			throw new Error('Es necesario ingresar el parametro de collection')
		}
		this.params.collection=cardParams.collection
            
            
		this.params.title=cardParams.title || ''
		if(this.params.contentType&&this.params.contentType!==''){
			throw new Error('Ya hay un tipo de contenido. No es posible agregar 2')
		}
		this.params.contentType='cards'

        //Content Options
		this.params.contentOptions={}
		this.params.contentOptions.cardAdd='js-add-'+this.params.tabId
		if(!cardParams.cardTitle){
			throw new Error('Es necesario ingresar el parametro de cardTitle')
		}
		this.params.contentOptions.cardTitle=cardParams.cardTitle
		this.params.cardId=cardParams.cardId || '_id'

        //exted extra params
		let extraParams = _.omit(cardParams, ['cardTitle','collection','title','cardId'])
		this.params.contentOptions=__.assignIn(this.params.contentOptions,extraParams)

        //Modal Options
		this.params.modalOptions=[]
            // for(let i=1;i<args.lenght;i++){
            //     if(typeof args[i] === "function"){
            //         args[i].apply()
            //     }
            // }
		return this
	}
	table(...args){
		let params={}
		if(!args||args.lenght===0){
			throw new Error('Es necesario agregar parametros')
		}
		let cardParams = args[0]

		// hacemos un prellenado de los params para evitar que se rompa funcionalidad, 
		// debido a que hay acciones que dependen de la estructura que se va a generar
		let testParams = typeof args[0] === 'function' ? args[0]({}, {}) : args[0]
		if(!testParams.collection){
			throw new Error('Es necesario ingresar el parametro de collection')
		}
		this.params.collection=testParams.collection
		if(!testParams.table){
			throw new Error('Es necesario ingresar una tabla')
		}
		this.params.table=testParams.table
		if(this.params.contentType&&this.params.contentType!==''){
			throw new Error('Ya hay un tipo de contenido. No es posible agregar 2')
		}
		this.params.contentType='table'
		this.params.title=testParams.title || ''
		this.params.contentOptions = {
			table           : this.params.table,
			id              : this.params.table+'List',
			selector        : {},
			tabularOptions	: {
				events: [
					
				]
			},
		}

		this.functionParams.push((routeParams, routeQuery) => {
			let params = {}
			if(typeof cardParams === 'function'){
				params=cardParams(routeParams, routeQuery)
			}else{
				params=cardParams
			}
			let extraParams = {}
			extraParams.contentOptions =  _.omit(params, ['table','collection','id','contentOptions'])
			return __.merge(this.params, extraParams)
		})

		//Modal Options
		this.params.modalOptions=[]
		return this
	}
	/** ToDo */
	custom(...args){
		let params={}
		if(!args||args.lenght===0){
			throw new Error('Es necesario agregar parametros')
		}
		let customParams = args[0]
		this.functionParams.push((routeParams, routeQuery) => {
			let params={}
			if(typeof customParams === 'function'){
				params=customParams(routeParams, routeQuery)
			}else{
				params=customParams
			}
			//exted extra params
			params = _.omit(params, ['tabId','selector'])
			return params=params
		})
		return this
	}
	newAction(...args){
		let action = new BMTAction(this.actions.length,this)
		this.actions.push(action)
		return action
	}
	checkboxes(...args){
		if(this.params.contentType==='table'){
			this.params.contentOptions.tabularOptions.checkbox=true
		}else{
			throw new Error('Solo se puede agregar checkbox a los contenidos tipo tabla')
		}
		return this
	}
	events(...args){
		if(!args||args.lenght===0){
			throw new Error('Es necesario agregar parametros')
		}
		for(let arg in args){
			let event=args[arg]
			for(let func in event){
				let funcName = this.getModule().getGenerator().uniqueId+'-'+this.params.tabId+'-func-'+this.metaParams.funcCount
				if(typeof event[func] === 'function'){
					this.params.events.push({
						name		: func,
						function	: funcName,
					})
					this.getModule().functions.push({
						name: funcName,
						function: event[func]
					})
				}else if(typeof event[func] === 'string'){
					this.params.events.push({
						name		: func,
						modal		: event[func],
					})
				}else if(typeof event[func] === 'object' && event[func].constructor === BMTAction){
					this.params.events.push({
						name		: func,
						modal		: event[func].metaParams.actionId,
						edit		: event[func].metaParams.isEdit
					})
				}
				this.metaParams.funcCount++
			}
		}
		return this
	}
	subscribe(...args){
		let params = {}
		if(!args||args.lenght===0){
			throw new Error('Es necesario agregar parametros')
		}
		params = args[0]
		if(params.constructor===Array){
			__.each(params,(v,k)=>{
				this.getModule().params.subscribes.push(v)
			})
		}else{
			this.params.subscribes.push(params)
		}
		
		return this
	}
	/** ToDo */
	setQuery(...args){
		let params={}
		if(!args||args.lenght===0){
			throw new Error(generalErrors.noParams)
		}
		this.params.query=args[0]
		return this
	}
	/** ToDo */
	setServerQuery(...args){
		let params={}
		if(!args||args.lenght===0){
			throw new Error(generalErrors.noParams)
		}
		this.params.serverQuery=args[0]
		return this
	}
	/** ToDo */
	setProjection(...args){
		let params={}
		if(!args||args.lenght===0){
			throw new Error(generalErrors.noParams)
		}
		this.params.query=projection[0]
		return this
	}
	/** ToDo */
	setServerProjection(...args){
		let params={}
		if(!args||args.lenght===0){
			throw new Error(generalErrors.noParams)
		}
		this.params.serverProjection=args[0]
		return this
	}
	/** ToDo (Funciones a ejecutarse cuando se cree el tab) */
	functions(...args){
		let functions = []
		if(!args||args.lenght===0){
			throw new Error(generalErrors.noParams)
		}
		for(let arg in args){
			let event=args[arg]
			for(let func in event){
				let funcName = this.getModule().getGenerator().uniqueId+'-'+this.params.tabId+'-func-'+this.metaParams.funcCount
				functions.push(funcName)
				if(typeof event[func] === 'function'){
					this.params.events.push({
						name		: func,
						function	: funcName,
					})
					this.getModule().functions.push({
						name: funcName,
						function: event[func]
					})
				}else if(typeof event[func] === 'string'){
					this.params.events.push({
						name		: func,
						modal		: event[func],
					})
				}else if(typeof event[func] === 'object' && event[func].constructor === BMTAction){
					this.params.events.push({
						name		: func,
						modal		: event[func].metaParams.actionId,
						edit		: event[func].metaParams.isEdit
					})
				}
				this.metaParams.funcCount++
			}
		}
		this.params.functions=functions
		// console.log(this.params)
		return this
	}
}
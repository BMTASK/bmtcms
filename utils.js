export const afImageFileHook = (collection, schema, updateSchema) => ({
	before: {
		method (doc) {
			this.template.data.images = this.template.findAll('input.proyecto-image-input')
			this.template.data.files = this.template.findAll('.js-proyecto-file-input[type="file"]')
			doc.collection = collection
			doc.schema = schema
			return doc
		},
		'method-update' (doc) {
			if (doc.$set) {
				this.template.data.images = this.template.findAll('input.proyecto-image-input')
				this.template.data.files = this.template.findAll('.js-proyecto-file-input[type="file"]')
				if (!!this.template.data.images.length || !!this.template.data.files.length) {
					delete doc.$unset
				}
				doc.$set.collection = collection
				doc.$set.schema = updateSchema || schema
				return doc
			} else {
				return false
			}
		},
	},
	after: {
		method (error, result) {
			if (error) {
				console.log(error)
				Materialize.toast('Ocurrió un error en el servidor', 3500, 'toast-negative')
			} else if (result) {
				const inputImage    = this.template.data.images,
					inputFile      	= this.template.data.files
				// const inputImage    = this.template.findAll('input.proyecto-image-input'),
				//     inputFile       = this.template.findAll('.js-proyecto-file-input[type="file"]')
				if (!!inputImage) {
					for (file in inputImage) {
						const f = inputImage[file]
						if (f.files.length) {
							S3.upload(
								{
									files: f.files,
									path: result,
								},
								(errorS3, resultS3) => {
									if (errorS3) {
										console.log(errorS3, f.files)
										Materialize.toast('Ocurrió un error al subir el archivo', 3500, 'toast-negative')
									} else if (resultS3) {
										Meteor.call('setImage', collection, result, resultS3, (callError, callResult) => {
											if (callError) {
												console.log(callError)
												Materialize.toast('Ocurrió un error en el servidor', 3500, 'toast-negative')
											}
										})
									}
								}
							)
						}
					}
				}
				if (!!inputFile) {
					for (file in inputFile) {
						const f = inputFile[file],
							key = f.dataset.schemaKey
						if (f.files) {
							S3.upload(
								{
									files: f.files,
									path: result,
								},
								(errorS3, resultS3) => {
									if (errorS3) {
										console.log(errorS3, f.files)
										Materialize.toast('Ocurrió un error al subir el archivo', 3500, 'toast-negative')
									} else if (resultS3) {
										Meteor.call('setFile', collection, result, resultS3, key, (callError, callResult) => {
											if (callError) {
												console.log(callError)
												Materialize.toast('Ocurrió un error en el servidor', 3500, 'toast-negative')
											} else if (callResult) {
											}
										})
									}
								}
							)
						}
					}
				}
			}
		},
		'method-update' (error, result) {
			if (error) {
				console.log(error)
				Materialize.toast('Ocurrió un error en el servidor', 3500, 'toast-negative')
			} else if (result) {
				const inputImage    = this.template.data.images,
					inputFile       = this.template.data.files
				// const inputImage    = this.template.findAll('input.proyecto-image-input'),
				// 	inputFile       = this.template.findAll('.js-proyecto-file-input[type="file"]')
				if (!!inputImage) {
					for (file in inputImage) {
						const f = inputImage[file]
						if (f.files) {
							S3.upload(
								{
									files: f.files,
									path: result,
								},
								(errorS3, resultS3) => {
									if (errorS3) {
										console.log(errorS3, f.files)
										Materialize.toast('Ocurrió un error al subir el archivo', 3500, 'toast-negative')
									} else if (resultS3) {
										Meteor.call('setImage', collection, result, resultS3, (callError, callResult) => {
											if (callError) {
												console.log(callError)
												Materialize.toast('Ocurrió un error en el servidor', 3500, 'toast-negative')
											} else if (callResult) {
												if (this.currentDoc.imagen && this.currentDoc.imagen.relative_url) {
													S3.delete(
														this.currentDoc.imagen.relative_url,
														(errorS3, resultS3) => {
															if (errorS3) {
																console.log(errorS3, f.files)
																Materialize.toast('Ocurrió un error al subir el archivo', 3500, 'toast-negative')
															}
														}
													)
												}
											}
										})
									}
								}
							)
						}
					}
				}
				if (!!inputFile) {
					for (file in inputFile) {
						const f = inputFile[file],
							key = f.dataset.schemaKey
						if (f.files) {
							S3.upload(
								{
									files: f.files,
									path: result,
								},
								(errorS3, resultS3) => {
									if (errorS3) {
										console.log(errorS3, f.files)
										Materialize.toast('Ocurrió un error al subir el archivo', 3500, 'toast-negative')
									} else if (resultS3) {
										Meteor.call('setFile', collection, result, resultS3, key, (callError, callResult) => {
											if (callError) {
												console.log(callError)
												Materialize.toast('Ocurrió un error en el servidor', 3500, 'toast-negative')
											} else if (callResult) {
												if (this.currentDoc[key] && this.currentDoc[key].relative_url) {
													S3.delete(
														this.currentDoc[key].relative_url,
														(errorS3, resultS3) => {
															if (errorS3) {
																console.log(errorS3, f.files)
																Materialize.toast('Ocurrió un error al subir el archivo', 3500, 'toast-negative')
															}
														}
													)
												}
											}
										})
									}
								}
							)
						}
					}
				}
			}
		}
	},
	onSuccess (type, result) {
		Materialize.toast('Registro guardado con éxito', 3500, 'green')
		// console.log(this.template)
		// const modal = findParentContext('Template.proyectoModal', this.template.find(`#${this.formId}`))
		// modal.modalReady(false)
		// modal.editAutoForm(false)
		// Meteor.defer(() => {
		//     AutoForm._forceResetFormValues(this.formId)
		// })
	},
	onError (type, result) {},
})

export const localSchema = {}

schemaSetup = new SimpleSchema({
	collection: {
		type: String,
		optional: true,
		autoform: {
			hidden: true,
			label: false
		}
	},
	schema: {
		type: String,
		optional: true,
		autoform: {
			hidden: true,
			label: false
		}
	},
	setRole: {
		type: [String],
		optional: true,
		autoform: {
			hidden: true,
			label: false
		}
	},
	"setRole.$": {
		type: String,
		optional: true,
		autoform: {
			hidden: true,
			label: false
		}
	},
	other: {
		type: Object,
		optional: true,
		autoform: {
			hidden: true,
			label: false,
		},
		blackbox: true
	},
	modifier: {
		type: String,
		optional: true,
		autoform: {
			hidden: true,
			label: false
		}
	}
})

localSchema.excelUpload  = new SimpleSchema([
	schemaSetup,
	{
		excel: {
			type: String,
			optional: false,
			autoform: {
				label: false,
				type: "fileProyecto",
				inputTitle: "Excel",
				placeholder: "Ingresar Archivo de Excel (xls o csv)"
			}
		}
	}
])


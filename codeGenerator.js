//import { privateRoutes, publicRoutes } from '/lib/router/router.js'
import { BMTModule, BMTCore } from './index.js'
import { generalErrors, codeGeneratorErrors } from './codeErrors.js'
import { _ } from 'underscore'
import  __  from 'lodash'
import uniqid from 'uniqid'

export class BMT extends BMTCore{
	constructor(router, uniqueId){
		super()
		if(router){
			this.router=router
		}
		if(uniqueId){
			this.uniqueId=uniqueId
		}else{
			this.uniqueId=uniqid()
		}
		this.params={}
		this.params.functions={}
		this.params.routeType='private'
		this.params.roles={}
		this.params.roles.sideNav=['admin']
		this.params.roles.roles=['admin']
		this.params.created=false
		this.routeCreator = this.params.routeType==='private' ? this.router.privateRoutes : this.router.publicRoutes
		this.params.default={}
		this.params.default.contentData={
			options: {
				content:{
					reactiveTabs: true,
					subscribes:[],
					tabs:[]
				}
			}
		}
		this.params.contentData=this.params.default.contentData
	}
	private(){
		this.params.routeType='private'
		return this
	}
	public(){
		this.params.routeType='public'
		return this
	}
	reactiveTabs(reactive){
		if(reactive){
			this.params.contentData.options.content.reactiveTabs=true
		}else{
			this.params.contentData.options.content.reactiveTabs=false
		}
	}
	newModule(...args){
		let self = this
		if(!args||Object.keys(args).length===0){
			throw new Error('Es necesario agregar parametros')
		}
		this.params.route=args[0]
		if(typeof args[1] === 'object'){
			let params = args[1]
			if(params.name) this.params.name = params.name
			if(params.sideNavTitle) this.params.sideNavTitle = params.sideNavTitle
			if(params.iconType) this.params.iconType = params.iconType
			if(params.icon) this.params.icon = params.icon
			if (params.routeTitle) this.params.routeTitle = params.routeTitle
			if (params.submodule) this.params.submodule = params.submodule
			if (params.submoduleParent) this.params.submoduleParent = params.submoduleParent
			if (params.triggersEnter) this.params.triggersEnter = params.triggersEnter
		}
		this.module=new BMTModule(this)
		return this.module
	}
	initSimpleRoute(){
		this.params.contentData=this.params.default.contentData
		if(this.module&&this.module.functions){
			this.params.functions=[]
			__.each(this.module.functions,(v,k)=>{
				this.params.functions[v.name]=v.function
			})
		}
	}
	initSetModule(routeParams, routeQuery){
		if(this.module){
			this.params.contentData.options.titleOptions=this.module.params.titleOptions(routeParams, routeQuery)
			this.params.contentData.options.content.subscribes=this.module.params.subscribes
		}
	}
	initSetTabs(routeParams, routeQuery){
		let self = this
		if(this.module&&this.module.tabs){
			//console.log(this.module.tabs)
			self.params.contentData.options.content.tabs=[]
			for(const tab of this.module.tabs){
				const contentTemp = []
				let tabId = ""
				let selector = ""
				const contentPick = ['contentOptions', 'contentTemplate', 'contentType', 'events']
				let contentDataStore = {}
				let tabActions = {}
				for(const content of tab){
					let currentTabAction = {}
					let contentData = __.clone(content.getParams(routeParams, routeQuery))
					if(!tabId){
						tabId = contentData.tabId // fix Multiples contenidos afectan el id del tab
					}
					if(!selector){
						selector = contentData.selector // fix Multiples contenidos afectan el id del tab
					}
					delete contentData.tabId
					delete contentData.selector
					// console.log(contentData)
					contentDataStore = __.merge(__.omit(contentData, contentPick), __.omit(contentData, contentPick))
					// contentData = __.pick(contentData, contentPick)
					self.initSetActions(content.actions,
						currentTabAction,
						{routeParams, routeQuery}
					)
					// tabActions = Object.assign(tabActions, currentTabAction)
					tabActions = __.merge(tabActions, currentTabAction)
					// console.log(tabActions)
					contentTemp.push(contentData)
					// self.params.contentData.options.content.tabs[self.params.contentData.options.content.tabs.length-1].push(contentData)
				}
				contentDataStore.tabId = tabId
				contentDataStore.selector = selector
				if(contentTemp.length === 1){
					self.params.contentData.options.content.tabs.push(
						Object.assign(
							contentTemp[0], contentDataStore, tabActions
						)
					)
				}else{
					self.params.contentData.options.content.tabs.push(
						Object.assign(
							{contentType: 'multiple', contentOptions: contentTemp},
							contentDataStore,
							tabActions
						)
					)
				}
				// self.params.contentData.options.content.tabs.push(tab[0].getParams(routeParams, routeQuery))
				// self.initSetActions(content,
				// 	self.params.contentData.options.content.tabs[self.params.contentData.options.content.tabs.length-1],
				// 	{routeParams, routeQuery}
				// )
				// for(const actions in self.params.contentData.options.content.tabs){
				// 	self.initSetActions(actions.actions,
				// 		actions,
				// 		{routeParams, routeQuery}
				// 	)
				// }
			}
		}
	}
	initSetActions(actions,tab, routeparams){
		let self = this
		// console.log(tab)
		if(actions&&typeof actions === 'object'){
			tab.modalOptions=[]
			__.each(actions,function(v,k){
				if(v.params&&v.params.modal){
					// tab.modalOptions.push(v.params.modal)
					tab.modalOptions.push(v.getParams(routeparams.routeParams, routeparams.routeQuery))
				}
			})
		}
		//console.log(tab)
	}
	setPrivileges(){
		if(this.router){
			if(this.router.blazeRoutes){
				this.router.blazeRoutes.push(this.params.name)
			}
			if(this.params.routeType==='private'){
				this.router.privateRouteNames.push(this.params.name)
				if(!__.isEmpty(this.params.roles.roles)){
					this.router.roleMap.push({
						route		: this.params.name,
						roles		: this.params.roles.roles,
						sideNavDisp	: this.params.roles.sideNav
					})
				}
			}
		}
	}
	setRoles(...args){
		if(this.params.created){
			throw new Error(codeGeneratorErrors.created)
		}
		if(!args||args.lenght===0){
			throw new Error(generalErrors.noParams)
		}
		this.params.roles.roles=args[0]
	}
	setSideNav(...args){
		if(this.params.created){
			throw new Error(codeGeneratorErrors.created)
		}
		if(!args||args.lenght===0){
			throw new Error(generalErrors.noParams)
		}
		this.params.roles.sideNav=args[0]
	}
	create(){
		const self = this
		this.initSimpleRoute()
		// this.initSetModule()
		// this.initSetTabs()
		// console.log(this.params.contentData.options.content.tabs[0])
		// console.log(this.params.contentData)
		let simpleLayout = {
			name				: this.params.name || this.params.route,
			sideNavTitle		:this.params.sideNavTitle || '',
			iconType			: this.params.iconType || false,
			icon				: this.params.icon || '',
			submodule			: this.params.submodule || '',
			submoduleParent		: this.params.submoduleParent || '',
			triggersEnter		: this.params.triggersEnter || [],
			action (routeParams, routeQuery) {
				// self.initSimpleRoute()
				self.initSetModule(routeParams, routeQuery)
				self.initSetTabs(routeParams, routeQuery)
				BlazeLayout.render('homeLayout', {
					nav			: 'homeUser',
					content		: 'simpleListLayout',
					sideNav 	: 'homeUserSideNav',
					contentData	: self.params.contentData
				})
				DocHead.setTitle(self.params.routeTitle || self.params.name || self.params.route)
			},
			triggersExit: [
				(context, redirect) => {
					BlazeLayout.reset()
				}
			]
		}
		__.assignIn(simpleLayout,this.params.functions)
		this.routeCreator.route(this.params.route, simpleLayout)
		this.setPrivileges()
		this.params.created=true
	}
}

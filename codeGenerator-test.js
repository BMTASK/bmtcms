import { privateRoutes, publicRoutes } from '/lib/router/router.js'

export class BMT{
	constructor(){
		this.params={}
		this.params.routeType='private'
		this.params.contentData={
			options: {
				content:{
					reactiveTabs: true,
					subscribes:[],
					tabs:[]
				}
			}
		}
	}
	private(){
		this.params.routeType='private'
		return this
	}
	public(){
		this.params.routeType='public'
		return this
	}
	subscribe(...args){
		return this
	}
	eventos(...args){
		return this
	}
	setTitle(...args){
		let params={}
		if(!args||args.lenght===0){
			throw new Error('Es necesario agregar parametros')
		}
		let titleParams = args[0]
		if(!titleParams.title){
			throw new Error('Es necesario ingresar titulo')
		}
		params.title=titleParams.title
		if(!titleParams.template){
			params.template=titleParams.template
		}else{
			params.template='simpleListTitle'
		}
		this.params.contentData.options.titleOptions=params
		return this
	}
	cards(...args){
		let params={}
		if(!args||args.lenght===0){
			throw new Error('Es necesario agregar parametros')
		}
		console.log(args)
		let cardParams = args[0]
		if(!cardParams.collection){
			throw new Error('Es necesario ingresar el parametro de collection')
		}
		params.collection=cardParams.collection
		if(!cardParams.cardTitle){
			throw new Error('Es necesario ingresar el parametro de cardTitle')
		}
		params.cardTitle=cardParams.cardTitle
		params.cardTitle=cardParams.cardTitle || '_id'
		for(let i=1;i<args.lenght;i++){
			if(typeof args[i] === 'function'){
				args[i].apply()
			}
		}
		return this
	}
	newModule(...args){
		if(!args||Object.keys(args).length===0){
			throw new Error('Es necesario agregar parametros')
		}
		this.params.route=args[0]
		for(let i=1;i<Object.keys(args).length;i++){
			if(typeof args[i] === 'object'){
				let params = args[i]
				if(params.name) this.params.name = params.name
				if(params.sideNavTitle) this.params.name = params.sideNavTitle
				if(params.iconType) this.params.name = params.iconType
				if(params.icon) this.params.name = params.icon
			}else if(typeof args[i] === 'function'){
							//this[args[i]].call()
				let params={}
				console.log('new module')
				args[i].bind(params)
				args[i].call()
				// console.log(args[i].toString())
			}
		}
		return this
	}
	create(){
		let routeCreator = this.params.routeType==='private' ? privateRoutes : publicRoutes
		routeCreator.route(this.params.route, {
			name: this.params.name || this.params.route,
			sideNavTitle:this.params.sideNavTitle || '',
			iconType: this.params.iconType || false,
			icon: this.params.icon || '',
			action (routeParams, routeQuery) {
				BlazeLayout.render('homeLayout', {
					nav			: 'homeUser',
					content		: 'simpleListLayout',
					sideNav 	: 'homeUserSideNav',
					contentData	: this.params.contentData
				})
				let title = 'SDI ' + (this.params.name || this.params.route)
				DocHead.setTitle(title)
			},
			triggersExit: [
				(context, redirect) => {
					BlazeLayout.reset()
				}
			]
		})
	}
}